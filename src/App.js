import React, { useState } from "react";
import './App.css';

import axios from "axios";

function App() {

  //array hook that contain all the value available.
  const [valuesApi, setValuesApi] = useState([]);
  //text hook that contain the value typed by the user
  const [text, setText] = useState('0');
    //array hook that contain all the value available depend on the amount choosen by the user.
  const [cards, setCards] = useState([]);
  //conditionnal render hook that will diplayed the cards available (when the user choose closest amount)
  const [cardsDisplay, setCardsDisplay] = useState(false);
  //conditionnal render hook that will diplayed the amount closest available.
  const [display, setDisplay] = useState(false);

  const token = "tokenTest123";
  const shopId = "5";

  //function that get the value available in the api depend on the text
  //parameter (text input)
  const getValues = () => {
    setCards([])
    setCardsDisplay(false)
    setDisplay(false)
    axios.get(`http://localhost:3000/shop/${shopId}/search-combination`, {
      "headers": {
        Authorization: token
      },
      params: {
        amount: `${text}`
      }
    })
      .then(result => {
        const res = result.data;
        setValuesApi(res)
        if (res.equal === undefined) {
          setDisplay(true)
        }
      })
      .catch(err => console.log(err))
  };

  //function that will close the display hook to display the amount available with 
  //the amount choosen by the user
  const showCards = (val) => {
    if(valuesApi.floor.value === val){
      setCards(valuesApi.floor.cards)
      setCardsDisplay(true)
      setDisplay(false)
      setText(valuesApi.floor.value)
    }else if(valuesApi.ceil.value === val){
      setCards(valuesApi.ceil.cards)
      setCardsDisplay(true)
      setDisplay(false)
      setText(valuesApi.ceil.value)

    }
  };

  // function that take the actual value inside the text input
  // and reach the highest value closest to this value
  const getHigherValue = () => {
    const int = parseInt(text)
    axios.get(`http://localhost:3000/shop/${shopId}/search-combination`, {
      "headers": {
        Authorization: token
      },
      params: {
        amount: `${int +1}`
      }
    })
      .then(result => {
        const res = result.data;
        setText(res.ceil.value.toString())
        return res
      })
      .catch(err => console.log(err))  
  };

  // function that take the actual value inside the text input
  // and reach the lowest value closest to this value
  const getLowerValue = () => {
    const int = parseInt(text)
    axios.get(`http://localhost:3000/shop/${shopId}/search-combination`, {
      "headers": {
        Authorization: token
      },
      params: {
        amount: `${int -1}`
      }
    })
      .then(result => {
        const res = result.data;
        setText(res.floor.value.toString())
        return res
      })
      .catch(err => console.log(err)) 
  };

  return (
    <div className="App">
      <h2>Choissisez le montant:</h2>
      <div className="mainButtons">
      <div className="insideButtons">
      <input
        type="text"
        onChange={(e) => setText(e.target.value)}
        value={text} />
      <button type="button" onClick={() => getHigherValue()}>+</button>
      <button type="button" onClick={() => getLowerValue()}>-</button>
      </div>
      <button className="validateButton" type="button" onClick={() => getValues()}>Valider</button>
      </div>

      {valuesApi.equal && (
        <>
          <p>Votre montant est composé {valuesApi.equal.cards >= 1 ? "de la carte suivante" : "des cartes suivantes"} :
          </p>
          {valuesApi.equal.cards.map((val) => (
            <div className='amount'>
              <span className='amountItem'>{val}€</span>
            </div>
          ))}
        </>
      )}
      {display &&
        <>
          <div className='amount'>
            {valuesApi.floor && (
            <div className='amountItem'
              onClick={() => showCards(valuesApi.floor.value)}>
              {valuesApi.floor.value}€
            </div>
            )}
            {valuesApi.ceil && (
            <div className='amountItem'
              onClick={() => {showCards(valuesApi.ceil.value)}}>
              {valuesApi.ceil.value}€
            </div>
            )}
          </div>
        </>}
      {cardsDisplay &&
        <>
        <p>En fonction du montant séléctionné voici le(s) carte(s) :</p>
          {cards.map((ite) => (
            <div className='amountItem'>{ite}€</div>
          ))}
        </>
      }
    </div>
  );
}

export default App;